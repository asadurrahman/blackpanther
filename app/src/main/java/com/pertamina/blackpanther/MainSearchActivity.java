package com.pertamina.blackpanther;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainSearchActivity extends AppCompatActivity {
    private Toolbar toolbar;
    int mYear,mMonth,mDay;
    private ProgressDialog progressDialog;
    private ImageView ivStartDate, ivEndDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_search);
        final int tipeBisnis = getIntent().getIntExtra("tipeBisnis",0);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Search SPBU");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        final EditText etStartDate = (EditText) findViewById(R.id.etStartDate);
        final EditText etEndDate = (EditText) findViewById(R.id.etEndDate);
        ivStartDate = (ImageView) findViewById(R.id.imageView3);
        ivEndDate = (ImageView) findViewById(R.id.imageView2);
        TextView buttonSearch = (TextView) findViewById(R.id.buttonSearch);
        Spinner spPromote = (Spinner) findViewById(R.id.spPromote);
        final LinearLayout llProduct = (LinearLayout) findViewById(R.id.llProduct);
        final LinearLayout llService = (LinearLayout) findViewById(R.id.llService);
        final LinearLayout llViewSuggestion = (LinearLayout) findViewById(R.id.llViewSuggestion);
        spPromote.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1){
                    llProduct.setVisibility(View.VISIBLE);
                    llService.setVisibility(View.GONE);
                } else if (i==2){
                    llProduct.setVisibility(View.GONE);
                    llService.setVisibility(View.VISIBLE);
                } else {
                    llProduct.setVisibility(View.GONE);
                    llService.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        if (tipeBisnis== 0){
            buttonSearch.setText("Advanced Search");
        }else llViewSuggestion.setVisibility(View.GONE);

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                Date d = mcurrentDate.getTime();

                DatePickerDialog mDatePicker = new DatePickerDialog(MainSearchActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        mcurrentDate.set(selectedyear, selectedmonth, selectedday);

                        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
                        String date_m = df2.format(mcurrentDate.getTime());
                        etStartDate.setText(date_m);
                    }
                }, mYear, mMonth, mDay);
                //  mDatePicker.getDatePicker().setCalendarViewShown(false);
                mDatePicker.getDatePicker().setMinDate(d.getTime());
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });
        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                Date d = mcurrentDate.getTime();

                DatePickerDialog mDatePicker = new DatePickerDialog(MainSearchActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        mcurrentDate.set(selectedyear, selectedmonth, selectedday);

                        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
                        String date_m = df2.format(mcurrentDate.getTime());
                        etEndDate.setText(date_m);
                    }
                }, mYear, mMonth, mDay);
                //  mDatePicker.getDatePicker().setCalendarViewShown(false);
                mDatePicker.getDatePicker().setMinDate(d.getTime());
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });
        ivStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                Date d = mcurrentDate.getTime();

                DatePickerDialog mDatePicker = new DatePickerDialog(MainSearchActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        mcurrentDate.set(selectedyear, selectedmonth, selectedday);

                        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
                        String date_m = df2.format(mcurrentDate.getTime());
                        etStartDate.setText(date_m);
                    }
                }, mYear, mMonth, mDay);
                //  mDatePicker.getDatePicker().setCalendarViewShown(false);
                mDatePicker.getDatePicker().setMinDate(d.getTime());
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });

        ivEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);
                Date d = mcurrentDate.getTime();

                DatePickerDialog mDatePicker = new DatePickerDialog(MainSearchActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                        mcurrentDate.set(selectedyear, selectedmonth, selectedday);

                        SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
                        String date_m = df2.format(mcurrentDate.getTime());
                        etEndDate.setText(date_m);
                    }
                }, mYear, mMonth, mDay);
                //  mDatePicker.getDatePicker().setCalendarViewShown(false);
                mDatePicker.getDatePicker().setMinDate(d.getTime());
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentListSPBU = new Intent(getApplicationContext(), ListSpbuActivity.class);
                intentListSPBU.putExtra("tipeBisnis", tipeBisnis);
                setupDialogBar();
                startActivity(intentListSPBU);
            }
        });

        AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.autoTvAddress);
        String [] address = getResources().getStringArray(R.array.spbuCityAddress);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line,
                address);
        textView.setAdapter(adapter);
    }

    private void setupDialogBar() {
        progressDialog = new ProgressDialog(MainSearchActivity.this);
        progressDialog.setMessage("Analyzing Your Needs"); // Setting Message
        progressDialog.setTitle("Loading..."); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(7000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }).start();
    }

    @Override
    public boolean onSupportNavigateUp(){
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (progressDialog != null)
        progressDialog.dismiss();
    }
}
