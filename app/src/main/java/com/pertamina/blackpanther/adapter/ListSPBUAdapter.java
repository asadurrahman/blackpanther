package com.pertamina.blackpanther.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pertamina.blackpanther.R;
import com.pertamina.blackpanther.model.SPBUModel;

import java.util.List;

/**
 * Created by BRI on 4/17/2018.
 */


public class ListSPBUAdapter extends RecyclerView.Adapter<ListSPBUAdapter.RecyclerViewHolders> {

    private List<SPBUModel> itemList;
    private Context context;

    public ListSPBUAdapter(Context context, List<SPBUModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_list_spbu, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.spbuName.setText(itemList.get(position).getName());
        holder.spbuAddress.setText(itemList.get(position).getAddress());
        if (itemList.get(position).getTipe().equalsIgnoreCase("Pasti Pas"))
            holder.spbuType.setTextColor(Color.RED);
        holder.spbuType.setText(itemList.get(position).getTipe());
        holder.viewerSpbu.setText(String.valueOf(itemList.get(position).getViewer()));
        holder.areaSpbu.setText(String.valueOf(itemList.get(position).getArea())+"/m2");
        holder.availSpaceSpbu.setText(String.valueOf(itemList.get(position).getAreaSpace())+"/m2");
        holder.priceSpbu.setText(itemList.get(position).getPrice()+"/bln");
        holder.spbuPhoto.setImageResource(itemList.get(position).getPhoto());
        if (itemList.get(position).getPlaces() == 0){
            int rand = randomWithRange(1, 8);
            switch (rand){
                case 1:
                    holder.placesImage.setImageResource(R.drawable.ic_gas);
                    holder.placesImage.setTag(R.drawable.ic_gas);
                    break;
                case 2:
                    holder.placesImage.setImageResource(R.drawable.ic_atm_grey);
                    holder.placesImage.setTag(R.drawable.ic_atm_grey);
                    break;
                case 3:
                    holder.placesImage.setImageResource(R.drawable.ic_restaurant_grey);
                    holder.placesImage.setTag(R.drawable.ic_restaurant_grey);
                    break;
                case 4:
                    holder.placesImage.setImageResource(R.drawable.ic_carwash_grey);
                    holder.placesImage.setTag(R.drawable.ic_carwash_grey);
                    break;
                case 5:
                    holder.placesImage.setImageResource(R.drawable.ic_foodstall_grey);
                    holder.placesImage.setTag(R.drawable.ic_foodstall_grey);
                    break;
                case 6:
                    holder.placesImage.setImageResource(R.drawable.ic_barbershop_grey);
                    holder.placesImage.setTag(R.drawable.ic_barbershop_grey);
                    break;
                case 7:
                    holder.placesImage.setImageResource(R.drawable.ic_digitalads_grey);
                    holder.placesImage.setTag(R.drawable.ic_digitalads_grey);
                    break;
                case 8:
                    holder.placesImage.setImageResource(R.drawable.ic_ads_grey);
                    holder.placesImage.setTag(R.drawable.ic_ads_grey);
                    break;
            }
            holder.availableText.setText("Recommended");
            holder.availableText.setVisibility(View.VISIBLE);
            holder.availableText.setBackgroundResource(R.drawable.round_corner);
        }else{
            holder.placesImage.setImageResource(itemList.get(position).getPlaces());
        }

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder{

        public TextView spbuName;
        public TextView spbuAddress;
        public TextView spbuType;
        public TextView viewerSpbu;
        public TextView areaSpbu;
        public TextView availSpaceSpbu;
        public TextView priceSpbu;
        public TextView availableText;
        public ImageView spbuPhoto;
        public ImageView placesImage;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            spbuName = (TextView)itemView.findViewById(R.id.tvSpbuList);
            spbuAddress = (TextView)itemView.findViewById(R.id.tvAddressList);
            spbuType = (TextView)itemView.findViewById(R.id.tvPrimaList);
            viewerSpbu = (TextView)itemView.findViewById(R.id.tvViewerList);
            areaSpbu = (TextView)itemView.findViewById(R.id.tvAreaList);
            availSpaceSpbu = (TextView)itemView.findViewById(R.id.tvPlaceList);
            priceSpbu = (TextView)itemView.findViewById(R.id.tvPriceList);
            availableText = (TextView) itemView.findViewById(R.id.tvAvailable);
            spbuPhoto = (ImageView)itemView.findViewById(R.id.ivSPBUList);
            placesImage = (ImageView)itemView.findViewById(R.id.ivPlaceList);
        }
    }

    public int randomWithRange(int min, int max){
        int range = (max - min) + 1;

        return (int) (Math.random() * range) + min;
    }
}
