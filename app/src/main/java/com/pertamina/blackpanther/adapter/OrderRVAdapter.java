package com.pertamina.blackpanther.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pertamina.blackpanther.R;
import com.pertamina.blackpanther.model.OrderModel;

import java.util.List;

/**
 * Created by BRI on 4/17/2018.
 */


public class OrderRVAdapter extends RecyclerView.Adapter<OrderRVAdapter.RecyclerViewHolders> {

    private List<OrderModel> itemList;
    private Context context;

    public OrderRVAdapter(Context context, List<OrderModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_order, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.tvSpbuId.setText(itemList.get(position).getSpbuId());
        holder.tvAddress.setText(itemList.get(position).getAddress());
        holder.tvEndDate.setText(itemList.get(position).getEndDate());
        holder.tvDaysLeft.setText("15 days left");
        holder.tvOrderId.setText(itemList.get(position).getOrderId());
        holder.tvContractId.setText(itemList.get(position).getContractId());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder{

        public TextView tvSpbuId;
        public TextView tvAddress;
        public TextView tvEndDate;
        public TextView tvDaysLeft;
        public TextView tvOrderId;
        public TextView tvContractId;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            tvSpbuId = (TextView)itemView.findViewById(R.id.tvSpbu);
            tvAddress = (TextView)itemView.findViewById(R.id.tvAddress);
            tvEndDate = (TextView)itemView.findViewById(R.id.tvStartDate);
            tvDaysLeft = (TextView)itemView.findViewById(R.id.tvDaysLeft);
            tvOrderId = (TextView)itemView.findViewById(R.id.tvOrderId);
            tvContractId = (TextView)itemView.findViewById(R.id.tvContractId);
        }
    }
}
