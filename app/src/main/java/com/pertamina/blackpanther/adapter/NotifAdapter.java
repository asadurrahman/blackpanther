package com.pertamina.blackpanther.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pertamina.blackpanther.R;
import com.pertamina.blackpanther.model.NotifModel;

import java.util.List;

/**
 * Created by BRI on 4/22/2018.
 */


public class NotifAdapter extends RecyclerView.Adapter<NotifAdapter.RecyclerViewHolders> {

    private List<NotifModel> itemList;
    private Context context;

    public NotifAdapter(Context context, List<NotifModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notif, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        if (itemList.get(position).getCategory() == 3){
            holder.upload.setVisibility(View.VISIBLE);
        } else {
            holder.upload.setVisibility(View.GONE);
        }
        holder.information.setText(itemList.get(position).getInformation());
        holder.orderId.setText(itemList.get(position).getOrderId());
        holder.spbuId.setText(itemList.get(position).getSpbuId());
        holder.startDate.setText(String.valueOf(itemList.get(position).getStartDate()));
        holder.address.setText(String.valueOf(itemList.get(position).getAddress()));
        holder.productType.setText(String.valueOf(itemList.get(position).getProductType()));
        holder.price.setText(itemList.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder{

        public TextView information;
        public TextView orderId;
        public TextView spbuId;
        public TextView startDate;
        public TextView address;
        public TextView productType;
        public TextView price;
        public Button upload;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            information = (TextView) itemView.findViewById(R.id.tvInformation);
            orderId = (TextView)itemView.findViewById(R.id.tvOrderId);
            spbuId = (TextView)itemView.findViewById(R.id.tvSpbu);
            startDate = (TextView)itemView.findViewById(R.id.tvStartDate);
            address = (TextView)itemView.findViewById(R.id.tvAddress);
            productType = (TextView)itemView.findViewById(R.id.tvProductModel);
            price = (TextView)itemView.findViewById(R.id.tvAmount);
            upload = (Button) itemView.findViewById(R.id.btnUpload);
        }
    }
}
