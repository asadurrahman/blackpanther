package com.pertamina.blackpanther.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pertamina.blackpanther.model.MessageModel;
import com.pertamina.blackpanther.R;

import java.util.List;

/**
 * Created by BRI on 4/17/2018.
 */


public class NotifRVAdapter extends RecyclerView.Adapter<NotifRVAdapter.RecyclerViewHolders> {

    private List<MessageModel> itemList;
    private Context context;

    public NotifRVAdapter(Context context, List<MessageModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_notif, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.personName.setText(itemList.get(position).getName());
        holder.personAddress.setText(itemList.get(position).getAddress());
        holder.personPhoto.setImageResource(itemList.get(position).getPhotoId());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder{

        public TextView personName;
        public TextView personAddress;
        public ImageView personPhoto;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            personName = (TextView)itemView.findViewById(R.id.person_name);
            personAddress = (TextView)itemView.findViewById(R.id.person_address);
            personPhoto = (ImageView)itemView.findViewById(R.id.circleView);
        }
    }
}
