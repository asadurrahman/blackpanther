package com.pertamina.blackpanther.model;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.daimajia.slider.library.SliderLayout;
import com.pertamina.blackpanther.R;
import com.pertamina.blackpanther.adapter.MessageRVAdapter;
import com.pertamina.blackpanther.adapter.NotifRVAdapter;
import com.pertamina.blackpanther.adapter.OrderRVAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BRI on 4/19/2018.
 */


public class MyOrderFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private FrameLayout fragmentContainer;
    private RecyclerView recyclerView, rvMessage;
    private RecyclerView.LayoutManager layoutManager;
    private SliderLayout sliderLayout;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //private OnFragmentInteractionListener mListener;

    public MyOrderFragment() {
        // Required empty public constructor
    }

    public static MyOrderFragment newInstance(int index) {
        MyOrderFragment fragment = new MyOrderFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int fragment = getArguments().getInt("index", 0);
        if (fragment == 0) {
            View view = inflater.inflate(R.layout.fragment_message, container, false);
            initOnGoing(view);
            //initDemoSettings(view);
            return view;
        } else {
            View view = inflater.inflate(R.layout.fragment_message, container, false);
            initHistory(view);
            return view;
        }
    }

    private void initOnGoing(View view){
        fragmentContainer = view.findViewById(R.id.fragment_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        List<OrderModel> itemsData = getAllOrder();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        //willBeDisplayed();

        OrderRVAdapter adapter = new OrderRVAdapter(getActivity(), itemsData);
        recyclerView.setAdapter(adapter);
    }

    private void initHistory(View view){
        fragmentContainer = view.findViewById(R.id.fragment_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        List<OrderModel> itemsData = getAllOrder();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        //willBeDisplayed();

        OrderRVAdapter adapter = new OrderRVAdapter(getActivity(), itemsData);
        recyclerView.setAdapter(adapter);
    }

    private List<OrderModel> getAllOrder(){
        ProductTypeModel ptm = new ProductTypeModel("ATM", 1520);

        List<OrderModel> allItems = new ArrayList<>();
        allItems.add(new OrderModel("SPBU #34 123456 ", "Makassar, Jakarta Timur", "Order ID : #A8572091", "Contract ID : #B8556T1-75390", "1 Nov 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 523456", "Pulogadung, Jakarta Timur","Order ID : #Y8323221", "Contract ID : #G156A12-34691", "1 Des 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 512345", "Jatinegara, Jakarta Timur", "Order ID : #N2537891", "Contract ID : #F5174F9-98130", "5 Jan 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 523411", "Rawamangun, Jakarta Timur", "Order ID : #T5123531", "Contract ID : #L582U52-02371", "3 May 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 234235", "Cipinang, Jakarta Timur", "Order ID : #F4562467", "Contract ID : #M936I2-12769", "7 April 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 512345", "Jatinegara, Jakarta Timur", "Order ID : #N2537891", "Contract ID : #F5174F9-98130", "5 Jan 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 523411", "Rawamangun, Jakarta Timur", "Order ID : #T5123531", "Contract ID : #L582U52-02371", "3 May 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 234235", "Cipinang, Jakarta Timur", "Order ID : #F4562467", "Contract ID : #M936I2-12769", "7 April 2017", "2 April 2018", ptm, 2100, 1));

        return allItems;
    }
    /*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /*public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/
}
