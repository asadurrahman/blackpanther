package com.pertamina.blackpanther;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.pertamina.blackpanther.adapter.OrderRVAdapter;
import com.pertamina.blackpanther.adapter.RVAdapter;
import com.pertamina.blackpanther.adapter.RecyclerItemLongClickListener;
import com.pertamina.blackpanther.adapter.ViewPagerAdapter;
import com.pertamina.blackpanther.model.OrderModel;
import com.pertamina.blackpanther.model.ProductTypeModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


public class MainFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    private FrameLayout fragmentContainer;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SliderLayout sliderLayout;
    private SharedPreferences sharedPreferences;

    public static MainFragment newInstance(int index) {
        MainFragment fragment = new MainFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int fragment = getArguments().getInt("index", 0);
        if (fragment == 0) {
            View view = inflater.inflate(R.layout.fragment_home, container, false);
            initHome(view);
            //initDemoSettings(view);
            return view;
        } else if (fragment == 1){
            View view = inflater.inflate(R.layout.fragment_order, container, false);
            //initDemoList(view);
            initOrder(view);
            return view;
        } else if (fragment == 2){
            View view = inflater.inflate(R.layout.fragment_inbox, container, false);
            initInbox(view);
            return view;
        } else {
            View view = inflater.inflate(R.layout.fragment_history, container, false);
            initOrder(view);
            return view;
        }
    }

    private void initHome(View view){

        fragmentContainer = view.findViewById(R.id.fragment_container);
        sliderLayout = (SliderLayout) view.findViewById(R.id.slider);

        HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        url_maps.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        url_maps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        url_maps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Pertamax Turbo",R.drawable.ic_slidebar_1);
        file_maps.put("My Pertamina",R.drawable.ic_promo_pertamina);
        file_maps.put("Kartini Pertamina",R.drawable.ic_pelumas);
        file_maps.put("Pertamina GO", R.drawable.ic_slidebar_3);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(4000);
        sliderLayout.addOnPageChangeListener(this);

        SharedPreferences prefs = getContext().getSharedPreferences("shared1", MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        // Implementasi Button to MainSearchActivity
        LinearLayout llAtmHome= (LinearLayout) view.findViewById(R.id.llAtmHome);
        LinearLayout ivSuggestion = (LinearLayout) view.findViewById(R.id.ivSuggestion);
        llAtmHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainSearchActivity = new Intent(getActivity(), MainSearchActivity.class);
                mainSearchActivity.putExtra("tipeBisnis", 2);
                editor.putInt("tipeBisnis",2);
                editor.apply();
                startActivity(mainSearchActivity);
            }
        });

        ivSuggestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainSearchActivity = new Intent(getActivity(), MainSearchActivity.class);
                mainSearchActivity.putExtra("tipeBisnis", 0);
                editor.putInt("tipeBisnis",0);
                editor.apply();
                startActivity(mainSearchActivity);
            }
        });
        LinearLayout llNitrogenHome = (LinearLayout) view.findViewById(R.id.llNitrogenHome);
        llNitrogenHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainSearchActivity = new Intent(getActivity(), MainSearchActivity.class);
                mainSearchActivity.putExtra("tipeBisnis", 1);
                editor.putInt("tipeBisnis",1);
                editor.apply();
                startActivity(mainSearchActivity);
            }
        });
        LinearLayout llRestaurantHome = (LinearLayout) view.findViewById(R.id.llRestaurantHome);
        llRestaurantHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainSearchActivity = new Intent(getActivity(), MainSearchActivity.class);
                mainSearchActivity.putExtra("tipeBisnis", 3);
                editor.putInt("tipeBisnis",3);
                editor.apply();
                startActivity(mainSearchActivity);
            }
        });
        LinearLayout llCarWashHome = (LinearLayout) view.findViewById(R.id.llCarWashHome);
        llCarWashHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainSearchActivity = new Intent(getActivity(), MainSearchActivity.class);
                mainSearchActivity.putExtra("tipeBisnis", 4);
                editor.putInt("tipeBisnis",4);
                editor.apply();
                startActivity(mainSearchActivity);
            }
        });
        LinearLayout llFoodStallHome = (LinearLayout) view.findViewById(R.id.llFoodStallHome);
        llFoodStallHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainSearchActivity = new Intent(getActivity(), MainSearchActivity.class);
                mainSearchActivity.putExtra("tipeBisnis", 5);
                editor.putInt("tipeBisnis",5);
                editor.apply();
                startActivity(mainSearchActivity);
            }
        });
        LinearLayout llBarberShopHome = (LinearLayout) view.findViewById(R.id.llBarberShopHome);
        llBarberShopHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainSearchActivity = new Intent(getActivity(), MainSearchActivity.class);
                mainSearchActivity.putExtra("tipeBisnis", 6);
                editor.putInt("tipeBisnis",6);
                editor.apply();
                startActivity(mainSearchActivity);
            }
        });
        LinearLayout llDigitalAdHome = (LinearLayout) view.findViewById(R.id.llDigitalAdHome);
        llDigitalAdHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainSearchActivity = new Intent(getActivity(), MainSearchActivity.class);
                mainSearchActivity.putExtra("tipeBisnis", 7);
                editor.putInt("tipeBisnis",7);
                editor.apply();
                startActivity(mainSearchActivity);
            }
        });
        LinearLayout llNonDigitalAd = (LinearLayout) view.findViewById(R.id.llNonDigitalAd);
        llNonDigitalAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainSearchActivity = new Intent(getActivity(), MainSearchActivity.class);
                mainSearchActivity.putExtra("tipeBisnis", 8);
                editor.putInt("tipeBisnis",8);
                editor.apply();
                startActivity(mainSearchActivity);
            }
        });

        /*ListView l = (ListView) view.findViewById(R.id.transformers);
        l.setAdapter(new TransformerAdapter(getActivity()));
        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sliderLayout.setPresetTransformer(((TextView) view).getText().toString());
                Toast.makeText(getActivity(), ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private void initHistory(View view) {
        fragmentContainer = view.findViewById(R.id.fragment_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        List<OrderModel> itemsData = getAllOrder();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        //willBeDisplayed();

        OrderRVAdapter adapter = new OrderRVAdapter(getActivity(), itemsData);
        recyclerView.setAdapter(adapter);
    }

    private void initOrder(View view) {
        fragmentContainer = view.findViewById(R.id.fragment_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        final List<OrderModel> itemsData = getAllOrder();
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        //willBeDisplayed();

        OrderRVAdapter adapter = new OrderRVAdapter(getActivity(), itemsData);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemLongClickListener(getActivity(), recyclerView, new RecyclerItemLongClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), DetailOrderActivity.class);
                intent.putExtra("spbuId", itemsData.get(position).getSpbuId());
                intent.putExtra("address", itemsData.get(position).getAddress());
                intent.putExtra("orderId", itemsData.get(position).getOrderId());
                intent.putExtra("contractId", itemsData.get(position).getContractId());
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    private void initDemoList(View view) {

        fragmentContainer = view.findViewById(R.id.fragment_container);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        willBeDisplayed();
        ArrayList<String> itemsData = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            itemsData.add("Fragment " + getArguments().getInt("index", -1) + " / Item " + i);
        }

        RVAdapter adapter = new RVAdapter(itemsData);
        recyclerView.setAdapter(adapter);
    }

    private ViewPager viewPager;

    private TabLayout tabLayout;

    private void initInbox(View view){
        fragmentContainer = view.findViewById(R.id.fragment_container);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tablayout);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new MyInboxFragment().newInstance(0), "Notification");
        adapter.addFragment(new MyInboxFragment().newInstance(1), "Message");
        viewPager.setAdapter(adapter);
    }

    private List<OrderModel> getAllOrder(){
        ProductTypeModel ptm = new ProductTypeModel("ATM", 1520);

        List<OrderModel> allItems = new ArrayList<>();
        allItems.add(new OrderModel("SPBU #34 123456 ", "Jagakarsa, Jakarta Selatan", "Order ID : #A8572091", "Contract ID : #B856T1-75390", "1 Nov 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 523456", "Pulogadung, Jakarta Timur","Order ID : #Y8323221", "Contract ID : #G163A1-34691", "1 Des 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 512345", "Jatinegara, Jakarta Timur", "Order ID : #N2537891", "Contract ID : #F174F9-98130", "5 Jan 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 523411", "Rawamangun, Jakarta Timur", "Order ID : #T5123531", "Contract ID : #L582U5-02371", "3 May 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 234235", "Cipinang, Jakarta Timur", "Order ID : #F4562467", "Contract ID : #M936I2-12769", "7 April 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 512345", "Jatinegara, Jakarta Timur", "Order ID : #N2537891", "Contract ID : #F574F9-98130", "5 Jan 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 523411", "Rawamangun, Jakarta Timur", "Order ID : #T5123531", "Contract ID : #L582U2-02371", "3 May 2017", "2 April 2018", ptm, 2100, 1));
        allItems.add(new OrderModel("SPBU #34 234235", "Cipinang, Jakarta Timur", "Order ID : #F4562467", "Contract ID : #M936I2-12769", "7 April 2017", "2 April 2018", ptm, 2100, 1));

        return allItems;
    }

    public void refresh() {
        if (getArguments().getInt("index", 0) > 0 && recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        if (sliderLayout != null)
            sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onResume() {
        if (sliderLayout != null)
            sliderLayout.startAutoCycle();
        super.onResume();
    }

    /**
     * Called when a fragment will be displayed
     */
    public void willBeDisplayed() {
        // animate the content
        if (fragmentContainer != null) {
            Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            fragmentContainer.startAnimation(fadeIn);
        }
    }

    /**
     * Called when a fragment will be hidden
     */
    public void willBeHidden() {
        if (fragmentContainer != null) {
            Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            fragmentContainer.startAnimation(fadeOut);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {}

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(getActivity(),slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();
    }
}
