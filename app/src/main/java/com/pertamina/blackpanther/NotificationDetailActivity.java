package com.pertamina.blackpanther;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pertamina.blackpanther.adapter.ListSPBUAdapter;
import com.pertamina.blackpanther.adapter.NotifAdapter;
import com.pertamina.blackpanther.adapter.RecyclerItemLongClickListener;
import com.pertamina.blackpanther.model.NotifModel;
import com.pertamina.blackpanther.model.SPBUModel;

import java.util.ArrayList;
import java.util.List;

public class NotificationDetailActivity extends AppCompatActivity {
    private String tvInfo="";
    private Toolbar toolbar;
    private int cat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.activity_notification_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        cat = intent.getIntExtra("category", 0);
        String title="";
        int vButtonUpload=View.GONE;
        switch(cat){
            case 0:
                title = "Order Status";
                tvInfo = "Your order is sent and waiting for respond. We will inform you when it's done.";
                break;
            case 1:
                title = "Verification Process";
                tvInfo = "Your order is received and waiting for verification. We will inform you when it's done.";
                break;
            case 2:
                title = "Approval Process by MOR";
                tvInfo = "Your order has been received and being verified by MOR. We will notify you when it's approved.";
                break;
            case 3:
                title = "Design Approval";
                tvInfo = "Your design has been received and being verified by SPBU. We will notify you whether it is appropriate or not.";
                vButtonUpload = View.VISIBLE;
                break;
            case 4:
                title = "Your Contract";
                tvInfo = "tvInfo = \"Your order is sent and waiting for SPBU man respond. We will inform you when it's done.";
                vButtonUpload = View.VISIBLE;
                break;
            case 5:
                title = "Payment Status";
                tvInfo = "Please transfer the total amount to the following account if you are using bank transfer or you can use credit/debit.";
                break;
            case 6:
                title = "Tax Payment";
                tvInfo = "Before your Ads are being installed, our company will have to pay for the tax. We will inform you as soon as we had done it";
                break;
            case 7:
                title = "Succeed Contract";
                tvInfo = "Hooray! Your order has been successfully installed. Tap here to see the details of your ongoing order";
                break;
            case 8:
                title = "Ended Contract";
                tvInfo = "Thank you for giving us the opportunity to serve you. We appreciate your business and the confidence you have placed in us. Please call us if we can be of further assistance.";
                break;
            default:
                title = "";
                break;

        }

        setupRecyclerView();
        toolbar.setTitle(title);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }
    private void setupRecyclerView(){
        RecyclerView rv = (RecyclerView) findViewById(R.id.recycler_view);
        List<NotifModel> allItems = new ArrayList<>();
        allItems.add(new NotifModel(tvInfo, "Order ID : #A2372091", "SPBU #34 646711", "Tue, 1 April 2018", "Duren Sawit, Jakarta Timur", "Restaurant", "Rp 5.200.000", cat));

        Intent intent = getIntent();
        if (intent.hasExtra("spbuId")){
            Log.d("Notif Detail", "masuk");
            int tipeBisnis = intent.getIntExtra("tipeBisnis", 0);
            String spbuId = intent.getStringExtra("spbuId");
            String orderId = intent.getStringExtra("orderId");
            String spbuAddress = intent.getStringExtra("spbuAddress");
            String productType = getProductType(tipeBisnis);
            String price = intent.getStringExtra("price")+"";
            allItems.add(new NotifModel(tvInfo, "Order ID : "+orderId, spbuId, "Tue, 1 April 2018", spbuAddress, productType, price, cat));
        }
        NotifAdapter listSPBUAdapter = new NotifAdapter(this,  allItems);

        LinearLayoutManager llm;
        llm = new LinearLayoutManager(this);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(llm);
        rv.setAdapter(listSPBUAdapter);
        rv.addOnItemTouchListener(new RecyclerItemLongClickListener(this, rv, new RecyclerItemLongClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                /*Intent intent = new Intent(getApplicationContext(), DetailSPBUActivity.class);
                startActivity(intent);*/
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    private String getProductType(int cat){
        switch(cat){
            case 1:
                return "Nitrogen";
                //break;
            case 2:
                return "ATM";
                //break;
            case 3:
                return "Restaurant";
                //break;
            case 4:
                return "Car Wash";
                //break;
            case 5:
                return "Food Stalls";
                //break;
            case 6:
                return "BarberShop";
                //break;
            case 7:
                return "Digital Ads";
                //break;
            case 8:
                return "Non Digital Ads";
                //break;
            default:
                return "Non Digital Ads";
                //break;

        }
    }
}
