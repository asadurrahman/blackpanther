package com.pertamina.blackpanther;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailSPBUActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_spbu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setTitle("Detail SPBU");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        final int tipeBisnis = getIntent().getIntExtra("tipeBisnis", 0);
        final String spbuName = getIntent().getStringExtra("spbuName");
        final String spbuAddress = getIntent().getStringExtra("spbuAddress");
        final String viewerSpbu = getIntent().getStringExtra("viewerSpbu");
        final String areaSpbu = getIntent().getStringExtra("areaSpbu");
        final String price = getIntent().getStringExtra("price");

        TextView tvSpbuName = (TextView) findViewById(R.id.tvSpbuDetail);
        TextView tvSpbuAddress = (TextView) findViewById(R.id.tvAddressDetail);
        TextView tvViewer = (TextView) findViewById(R.id.tvViewerDetail);
        TextView tvArea = (TextView) findViewById(R.id.tvAreaDetail);
        TextView tvSpbuName2 = (TextView) findViewById(R.id.nameSpbu);
        TextView tvSpbuAddress2 = (TextView) findViewById(R.id.tvAddressDetail2);

        tvSpbuName.setText(spbuName);
        tvSpbuName2.setText(spbuName);
        tvSpbuAddress.setText(spbuAddress);
        tvSpbuAddress2.setText(spbuAddress);
        tvViewer.setText(viewerSpbu);
        tvArea.setText(areaSpbu);

        Button buttonOrder = (Button) findViewById(R.id.buttonOrder);
        buttonOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentAttach = new Intent(getApplicationContext(), AttachmentActivity.class);
                intentAttach.putExtra("tipeBisnis",tipeBisnis);
                intentAttach.putExtra("spbuName", spbuName);
                intentAttach.putExtra("spbuAddress", spbuAddress);
                intentAttach.putExtra("viewerSpbu", viewerSpbu);
                intentAttach.putExtra("areaSpbu", areaSpbu);
                intentAttach.putExtra("price", price);
                setupDialogBar();
                startActivity(intentAttach);
            }
        });


    }

    private void setupDialogBar() {
        progressDialog = new ProgressDialog(DetailSPBUActivity.this);
        progressDialog.setMessage("Waiting for server response"); // Setting Message
        progressDialog.setTitle("Loading..."); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(7000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (progressDialog != null)
            progressDialog.dismiss();
    }

}
