package com.pertamina.blackpanther;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class SuccessfulOrderActivity extends AppCompatActivity {

    private ImageView ivIconSuccessful;
    private TextView tvPlaceSuccessful;
    private TextView tvSpbuSuccessful;
    private TextView tvOrderIdSuccessful;
    private Button btnDetailOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_successful_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setTitle("Order Successful!");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        Random rand = new Random();
        final long pickedNumber = rand.nextInt(10000000) + 1000000;
        final int tipeBisnis = getIntent().getIntExtra("tipeBisnis", 0);
        final String spbuName = getIntent().getStringExtra("spbuName");
        final String spbuAddress = getIntent().getStringExtra("spbuAddress");
        final String price = getIntent().getStringExtra("price");

        Log.d("Successful", price);

        ivIconSuccessful = (ImageView) findViewById(R.id.ivIconSuccessful);
        tvPlaceSuccessful = (TextView) findViewById(R.id.tvPlaceSuccessful);
        tvSpbuSuccessful = (TextView) findViewById(R.id.tvSpbuSuccessful);
        tvOrderIdSuccessful = (TextView) findViewById(R.id.tvOrderIdSuccessful);
        btnDetailOrder = (Button) findViewById(R.id.btnOrderDetail);

        tvSpbuSuccessful.setText(spbuName);
        tvOrderIdSuccessful.setText("Your Order ID : #"+pickedNumber);

        if(tipeBisnis==1) {
            ivIconSuccessful.setImageResource(R.drawable.ic_nitrogen);
            tvPlaceSuccessful.setText("Gas Spot");
        }
        else if (tipeBisnis==2){
            ivIconSuccessful.setImageResource(R.drawable.ic_atm_96);
            tvPlaceSuccessful.setText("ATM Spot");
        }
        else if (tipeBisnis==3) {
            ivIconSuccessful.setImageResource(R.drawable.ic_restaurant_3_96);
            tvPlaceSuccessful.setText("Restaurant Spot");
        }
        else if (tipeBisnis==4) {
            ivIconSuccessful.setImageResource(R.drawable.ic_car_wash_96);
            tvPlaceSuccessful.setText("Car Wash Spot");
        }
        else if (tipeBisnis==5) {
            ivIconSuccessful.setImageResource(R.drawable.ic_fb_stalls_96);
            tvPlaceSuccessful.setText("Food Stall Spot");
        }
        else if (tipeBisnis==6) {
            ivIconSuccessful.setImageResource(R.drawable.ic_barber_shop);
            tvPlaceSuccessful.setText("Barbershop Spot");

        }
        else if (tipeBisnis==7) {
            ivIconSuccessful.setImageResource(R.drawable.ic_digital_ad);
            tvPlaceSuccessful.setText("Digital Ads");
        }
        else{
            ivIconSuccessful.setImageResource(R.drawable.ic_non_digital);
            tvPlaceSuccessful.setText("Non-digital Ads");
        }

        ivIconSuccessful.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SuccessfulOrderActivity.this, NotificationDetailActivity.class);
                intent.putExtra("spbuId", spbuName);
                intent.putExtra("orderId", tvOrderIdSuccessful.getText().toString());
                intent.putExtra("spbuAddress", spbuAddress);
                intent.putExtra("tipeBisnis", tipeBisnis);
                intent.putExtra("price", price+"");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);

            }
        });

        btnDetailOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SuccessfulOrderActivity.this, NotificationDetailActivity.class);
                intent.putExtra("spbuId", spbuName);
                intent.putExtra("orderId", pickedNumber+"");
                intent.putExtra("spbuAddress", spbuAddress);
                intent.putExtra("tipeBisnis", tipeBisnis);
                intent.putExtra("price", price);


                startActivity(intent);

            }
        });
    }

}
