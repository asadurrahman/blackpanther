package com.pertamina.blackpanther;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.daimajia.slider.library.SliderLayout;
import com.pertamina.blackpanther.adapter.MessageRVAdapter;
import com.pertamina.blackpanther.adapter.NotifRVAdapter;
import com.pertamina.blackpanther.adapter.RVAdapter;
import com.pertamina.blackpanther.adapter.RecyclerItemLongClickListener;
import com.pertamina.blackpanther.model.MessageModel;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyInboxFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyInboxFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyInboxFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private FrameLayout fragmentContainer;
    private RecyclerView recyclerView, rvMessage;
    private RecyclerView.LayoutManager layoutManager;
    private SliderLayout sliderLayout;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //private OnFragmentInteractionListener mListener;

    public MyInboxFragment() {
        // Required empty public constructor
    }

    public static MyInboxFragment newInstance(int index) {
        MyInboxFragment fragment = new MyInboxFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int fragment = getArguments().getInt("index", 0);
        if (fragment == 0) {
            View view = inflater.inflate(R.layout.fragment_message, container, false);
            initNotification(view);
            //initDemoSettings(view);
            return view;
        } else {
            View view = inflater.inflate(R.layout.fragment_message, container, false);
            initMessageList(view);
            return view;
        }
    }

    private void initNotification(View view){
        fragmentContainer = view.findViewById(R.id.fragment_container);
        rvMessage = (RecyclerView) view.findViewById(R.id.recycler_view);
        List<MessageModel> rowListItem = getAllNotif();
        layoutManager = new LinearLayoutManager(getActivity());
        rvMessage.setLayoutManager(layoutManager);

        rvMessage.addOnItemTouchListener(
                new RecyclerItemLongClickListener(getActivity(), rvMessage, new RecyclerItemLongClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // TODO Handle item click

                        Intent myIntent = new Intent(getActivity(), NotificationDetailActivity.class);
                        int cat = position;
                        myIntent.putExtra("category", cat);

                        startActivity(myIntent);
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {

                    }
                })
        );

        NotifRVAdapter rvAdapter = new NotifRVAdapter(getActivity(), rowListItem);
        rvMessage.setAdapter(rvAdapter);
    }

    private void initMessageList(View view){
        fragmentContainer = view.findViewById(R.id.fragment_container);
        rvMessage = (RecyclerView) view.findViewById(R.id.recycler_view);
        List<MessageModel> rowListItem = getAllMessage();
        layoutManager = new LinearLayoutManager(getActivity());
        rvMessage.setLayoutManager(layoutManager);

        MessageRVAdapter rvAdapter = new MessageRVAdapter(getActivity(), rowListItem);
        rvMessage.setAdapter(rvAdapter);
    }

    private List<MessageModel> getAllMessage(){

        List<MessageModel> allItems = new ArrayList<MessageModel>();
        allItems.add(new MessageModel("Rian Fitriansyah", "Data-data sedang kami validasi. Mohon...", R.drawable.ic_profpic_1));
        allItems.add(new MessageModel("Anita Rohmawati", "Pemesanan digital ads untuk tanggal 12...", R.drawable.ic_profpic_2));
        allItems.add(new MessageModel("Amiarsih Hapsari", "Berikut dilampirkan persyaratan yang harus...", R.drawable.ic_profpic_3));
        allItems.add(new MessageModel("Asadurrahman Al Qayyim", "Space untuk atm center pada SPBU Jagakarsa..", R.drawable.ic_profpic_4));
        allItems.add(new MessageModel("Najla Nadhia Rahmawati", "Untuk pemasangan baliho pada tanggal...", R.drawable.ic_profpic_5));
        allItems.add(new MessageModel("Farhan Syakir", "San Fransisco, United States", R.drawable.ic_profpic_6));
        allItems.add(new MessageModel("Denny Yoesoef", "Italion Gata, Padova, Italy", R.drawable.ic_profpic_2));
        allItems.add(new MessageModel("Sheila Rizky Melati", "San Fransisco, United States", R.drawable.ic_profpic_1));

        return allItems;
    }

    private List<MessageModel> getAllNotif(){

        List<MessageModel> allItems = new ArrayList<MessageModel>();
        allItems.add(new MessageModel("Order Status", "Your current order status", R.drawable.ic_order_status));
        allItems.add(new MessageModel("Verification by SPBU", "You will be notified here if your order is being verified", R.drawable.ic_verification_2));
        allItems.add(new MessageModel("Approval by MOR", "You will be informed here if your order is being verify by MOR", R.drawable.ic_approval));
        allItems.add(new MessageModel("Design Approval", "You have to send and wait for design approval", R.drawable.ic_approval));
        allItems.add(new MessageModel("Contract Signed", "You will be notified if you have new contract to sign", R.drawable.ic_contract));
        allItems.add(new MessageModel("Payment Status", "Your payment status will be informed here", R.drawable.ic_payment_status_2));
        allItems.add(new MessageModel("Ad Tax Payment", "Tax payment process for ads by SPBU", R.drawable.ic_tax));
        allItems.add(new MessageModel("Contract On Going", "Your on going contract will be notified here", R.drawable.ic_ads_installed_green));
        allItems.add(new MessageModel("Contract Ended", "We will notify your ended contract", R.drawable.ic_ads_ended));

        return allItems;
    }


    /*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /*public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/
}
