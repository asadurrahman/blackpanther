package com.pertamina.blackpanther;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

public class DetailOrderActivity extends AppCompatActivity {

    private TextView tvSpbu, tvAddress, tvOrderId, tvContractId, tvAverageViewer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Ongoing Order");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        Intent intent = getIntent();
        String spbuId = intent.getStringExtra("spbuId");
        String address = intent.getStringExtra("address");
        String orderId = intent.getStringExtra("orderId");
        String contractId = intent.getStringExtra("contractId");
        LineChart lineChart = (LineChart) findViewById(R.id.chart);
        tvSpbu = (TextView) findViewById(R.id.tvSpbu);
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvOrderId = (TextView) findViewById(R.id.tvOrderId);
        tvContractId = (TextView) findViewById(R.id.tvContractId);
        tvAverageViewer = (TextView) findViewById(R.id.tvAverageViewer);

        tvSpbu.setText(spbuId);
        tvAddress.setText(address);
        tvOrderId.setText(orderId);
        tvContractId.setText(contractId);
        LineDataSet set1;

        ArrayList<Entry> values = new ArrayList<Entry>();

        float average = 0;

        for (int i = 0; i < 7; i++){
            int val = randomWithRange(1000,5000);
            average = average + val;
            values.add (new Entry(i, val, getResources().getDrawable(R.drawable.ic_star)));
        }

        average = average / 7;
        int aver = (int) average;
        tvAverageViewer.setText(String.valueOf(aver)+" Visitor Daily");

        set1 = new LineDataSet(values, "Viewer");

        set1.setDrawIcons(false);

        // set the line to be drawn like this "- - - - - -"
        set1.enableDashedLine(10f, 5f, 0f);
        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(true);
        set1.setFormLineWidth(1f);
        set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set1.setFormSize(15.f);
        set1.setFillColor(getResources().getColor(R.color.colorPrimary));

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(dataSets);

        // set data
        lineChart.setData(data);
    }

    public int randomWithRange(int min, int max){
        int range = (max - min) + 1;

        return (int) (Math.random() * range) + min;
    }

}
