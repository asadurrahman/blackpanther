package com.pertamina.blackpanther;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pertamina.blackpanther.adapter.ListSPBUAdapter;
import com.pertamina.blackpanther.adapter.RecyclerItemLongClickListener;
import com.pertamina.blackpanther.model.SPBUModel;

import java.util.ArrayList;
import java.util.List;

public class ListSpbuActivity extends AppCompatActivity {

    int drawablePlace;
    String price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_spbu);
        final int tipeBisnis = getIntent().getIntExtra("tipeBisnis",0);

        if (tipeBisnis == 0) drawablePlace = 0;
        else if (tipeBisnis == 1) {drawablePlace = R.drawable.ic_gas; price = "Rp. 1.500.000";}
        else if (tipeBisnis == 2) {drawablePlace = R.drawable.ic_atm_grey; price = "Rp.5.000.000";}
        else if (tipeBisnis == 3) {drawablePlace = R.drawable.ic_restaurant_grey; price = "Rp. 7.500.000";}
        else if (tipeBisnis == 4) {drawablePlace = R.drawable.ic_carwash_grey; price = "Rp. 5.000.000";}
        else if (tipeBisnis == 5) {drawablePlace = R.drawable.ic_foodstall_grey; price = "Rp. 6.000.000";}
        else if (tipeBisnis == 6) {drawablePlace = R.drawable.ic_barbershop_grey; price = "Rp. 3.500.000";}
        else if (tipeBisnis == 7) {drawablePlace = R.drawable.ic_digitalads_grey; price = "Rp. 1.500.000";}
        else {drawablePlace = R.drawable.ic_ads_grey; price = "Rp. 1.500.000";}

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("SPBU");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        RecyclerView recyclerViewSpbu = (RecyclerView) findViewById(R.id.recycler_view_spbu);
        List<SPBUModel> allItems = new ArrayList<SPBUModel>();
        price = "Rp 5.500.000";
        allItems.add(new SPBUModel(0, "SPBU 31-11403",
                "Jelambar, Grogol Petamburan, Jakarta Barat 11460",
                "PASTI PRIMA", 2500, 2000, 200, price, R.drawable.spbu_pasti_prima_2,
                drawablePlace));
        price = "Rp 4.800.000";
        allItems.add(new SPBUModel(1, "SPBU 31-11404",
                "Jati Pulo, Palmerah, Jakarta Barat 11430",
                "PASTI PRIMA", 2000, 1800, 150, price, R.drawable.spbu_pasti_prima_2,
                drawablePlace));
        price = "Rp 6.200.000";
        allItems.add(new SPBUModel(2, "SPBU 31-11802",
                "Kalideres, Jakarta Barat 11840",
                "PASTI PAS", 3000, 2300, 250, price, R.drawable.spbu_pasti_pas,
                drawablePlace));
        price = "Rp 5.700.000";
        allItems.add(new SPBUModel(3, "SPBU 31-11301",
                "Wijaya Kusuma, Grogol Petamburan, Jakarta Barat 11460",
                "PASTI PAS", 3100, 2500, 230, price, R.drawable.spbu_pasti_pas,
                drawablePlace));
        price = "Rp 7.100.000";
        allItems.add(new SPBUModel(4, "SPBU 31-11103",
                "Slipi,  Palmerah, Jakarta Barat 11410",
                "PASTI PRIMA", 2500, 3000, 175, price, R.drawable.spbu_pasti_prima_2,
                drawablePlace));
        ListSPBUAdapter listSPBUAdapter = new ListSPBUAdapter(this,  allItems);

        LinearLayoutManager llm;
        llm = new LinearLayoutManager(this);
        recyclerViewSpbu.setHasFixedSize(true);
        recyclerViewSpbu.setLayoutManager(llm);
        recyclerViewSpbu.setAdapter(listSPBUAdapter);
        recyclerViewSpbu.addOnItemTouchListener(new RecyclerItemLongClickListener(this, recyclerViewSpbu, new RecyclerItemLongClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getApplicationContext(), DetailSPBUActivity.class);
                TextView spbuName = (TextView) view.findViewById(R.id.tvSpbuList);
                TextView spbuAddress = (TextView) view.findViewById(R.id.tvAddressList);
                TextView viewerSpbu = (TextView) view.findViewById(R.id.tvViewerList);
                TextView areaSpbu = (TextView) view.findViewById(R.id.tvAreaList);

                if (tipeBisnis != 0) intent.putExtra("tipeBisnis",tipeBisnis);
                else {
                    ImageView imageView = (ImageView) view.findViewById(R.id.ivPlaceList);
                    int placeList = (Integer) imageView.getTag();

                    switch (placeList){
                        case R.drawable.ic_gas: intent.putExtra("tipeBisnis",1); break;
                        case R.drawable.ic_atm_grey: intent.putExtra("tipeBisnis",2); break;
                        case R.drawable.ic_restaurant_grey: intent.putExtra("tipeBisnis",3); break;
                        case R.drawable.ic_carwash_grey: intent.putExtra("tipeBisnis",4); break;
                        case R.drawable.ic_foodstall_grey: intent.putExtra("tipeBisnis",5); break;
                        case R.drawable.ic_barbershop_grey: intent.putExtra("tipeBisnis",6); break;
                        case R.drawable.ic_digitalads_grey: intent.putExtra("tipeBisnis",7); break;
                        case R.drawable.ic_ads_grey: intent.putExtra("tipeBisnis",8); break;
                    }
                }
                intent.putExtra("spbuName", spbuName.getText());
                intent.putExtra("spbuAddress", spbuAddress.getText());
                intent.putExtra("viewerSpbu", viewerSpbu.getText());
                intent.putExtra("areaSpbu", areaSpbu.getText());
                intent.putExtra("price", price);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

}
