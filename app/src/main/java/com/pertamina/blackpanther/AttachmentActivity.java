package com.pertamina.blackpanther;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class AttachmentActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attachment_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setTitle("Mandatory Documents");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        final int tipeBisnis = getIntent().getIntExtra("tipeBisnis", 0);
        final String spbuName = getIntent().getStringExtra("spbuName");
        final String spbuAddress = getIntent().getStringExtra("spbuAddress");
        final String price = getIntent().getStringExtra("price");

        Button buttonOrder = (Button) findViewById(R.id.buttonAttach);
        buttonOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNotification();
                Intent intentSuccess= new Intent(getApplicationContext(), SuccessfulOrderActivity.class);
                intentSuccess.putExtra("tipeBisnis", tipeBisnis);
                intentSuccess.putExtra("spbuName", spbuName);
                intentSuccess.putExtra("spbuAddress", spbuAddress);
                intentSuccess.putExtra("price", price);
                setupDialogBar();
                startActivity(intentSuccess);
            }
        });

    }

    private void addNotification() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle("Pertamina Ads Station")
                .setContentText("Your order has been placed and waiting for verification from SPBU officer!")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Click here to see your order"))
                .setPriority(NotificationCompat.PRIORITY_HIGH);
        /*NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.)
                        .setContentTitle("Notifications Example")
                        .setContentText("This is a test notification");*/

        Intent notificationIntent = new Intent(this, NotificationDetailActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, mBuilder.build());
    }

    private void setupDialogBar() {
        progressDialog = new ProgressDialog(AttachmentActivity.this);
        progressDialog.setMessage("Submitting your attachment"); // Setting Message
        progressDialog.setTitle("Loading..."); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(10000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (progressDialog != null)
            progressDialog.dismiss();
    }

}
