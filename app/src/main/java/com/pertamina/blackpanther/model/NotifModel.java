package com.pertamina.blackpanther.model;

/**
 * Created by BRI on 4/22/2018.
 */

public class NotifModel {
    private String information;
    private String orderId;
    private String spbuId;
    private String startDate;
    private String address;
    private String productType;
    private String price;
    private int category;

    public NotifModel(String information, String orderId, String spbuId, String startDate, String address, String productType, String price, int category) {
        this.information = information;
        this.orderId = orderId;
        this.spbuId = spbuId;
        this.startDate = startDate;
        this.address = address;
        this.productType = productType;
        this.price = price;
        this.category = category;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSpbuId() {
        return spbuId;
    }

    public void setSpbuId(String spbuId) {
        this.spbuId = spbuId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
