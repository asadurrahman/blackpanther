package com.pertamina.blackpanther.model;

/**
 * Created by BRI on 4/18/2018.
 */

public class HistoryModel {

    private String spbuId;
    private String address;
    private String orderId;
    private String contractId;
    private String startDate;
    private String endDate;
    private ProductTypeModel productType;
    private int luasArea;
    private int photoId;

    public HistoryModel(String spbuId, String address, String orderId, String contractId,
                        String startDate, String endDate, ProductTypeModel productType,
                        int luasArea, int photoId) {
        this.spbuId = spbuId;
        this.address = address;
        this.orderId = orderId;
        this.contractId = contractId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.productType = productType;
        this.luasArea = luasArea;
        this.photoId = photoId;
    }

    public String getSpbuId() {
        return spbuId;
    }

    public void setSpbuId(String spbuId) {
        this.spbuId = spbuId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ProductTypeModel getProductType() {
        return productType;
    }

    public void setProductType(ProductTypeModel productType) {
        this.productType = productType;
    }

    public int getLuasArea() {
        return luasArea;
    }

    public void setLuasArea(int luasArea) {
        this.luasArea = luasArea;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }
}
