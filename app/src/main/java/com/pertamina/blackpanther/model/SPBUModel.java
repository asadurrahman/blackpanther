package com.pertamina.blackpanther.model;

public class SPBUModel {

    private int id;
    private String name;
    private String address;
    private String tipe;
    private int viewer;
    private int area;
    private int areaSpace;
    private String price;
    private int photo;
    private int places;

    public SPBUModel (int id, String name, String address, String tipe, int viewer, int area,
                      int areaSpace, String price, int photo, int places){
        this.id = id;
        this.name = name;
        this.address = address;
        this.tipe = tipe;
        this.viewer = viewer;
        this.area = area;
        this.areaSpace = areaSpace;
        this.price = price;
        this.photo = photo;
        this.places = places;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public int getViewer() {
        return viewer;
    }

    public void setViewer(int viewer) {
        this.viewer = viewer;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getAreaSpace() {
        return areaSpace;
    }

    public void setAreaSpace(int areaSpace) {
        this.areaSpace = areaSpace;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}