package com.pertamina.blackpanther.model;

/**
 * Created by BRI on 4/17/2018.
 */

public class MessageModel {

    private String name;
    private String address;
    private int photoId;

    public MessageModel(String name, String address, int photoId) {
        this.name = name;
        this.address = address;
        this.photoId = photoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }
}
