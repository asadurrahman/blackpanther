package com.pertamina.blackpanther.model;

/**
 * Created by BRI on 4/18/2018.
 */

public class ProductTypeModel {
    String type;
    int viewer;

    public ProductTypeModel(String type, int viewer) {
        this.type = type;
        this.viewer = viewer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getViewer() {
        return viewer;
    }

    public void setViewer(int viewer) {
        this.viewer = viewer;
    }
}
